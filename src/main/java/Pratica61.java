import java.util.HashMap;
import utfpr.ct.dainf.if62c.pratica.*;
/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {
    public static void main(String[] args) {
        Time gremio = new Time();
        gremio.addJogador("Goleiro", new Jogador(1, "Grohe"));
        gremio.addJogador("Zagueiro", new Jogador(3, "Bressan"));
        gremio.addJogador("Lateral", new Jogador(7, "Douglas"));
        gremio.addJogador("Atacante", new Jogador(11, "Luan"));
        
        Time inter = new Time();
        inter.addJogador("Lateral", new Jogador(7, "Valdemir"));
        inter.addJogador("Atacante", new Jogador(11, "Roberson"));
        inter.addJogador("Goleiro", new Jogador(1, "Daniel"));
        inter.addJogador("Zagueiro", new Jogador(3, "Klaus"));
        
        System.out.println("Posição     Time 1    Time 2");
        
        HashMap<String, Jogador> jogadoresGremio = gremio.getJogadores();
        HashMap<String, Jogador> jogadoresInter = inter.getJogadores();
        
        for(String chave : jogadoresGremio.keySet()){
            Jogador gr = jogadoresGremio.get(chave);
            Jogador in = jogadoresInter.get(chave);
            System.out.println(chave + "    " + gr.toString() + "   " + in.toString());
        }
        
    }
}
