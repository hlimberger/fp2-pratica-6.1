/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.HashMap;

/**
 *
 * @author henrique
 */
public class Time {
    private HashMap<String, Jogador> time;
    
    public Time(){
        this.time = new HashMap<>();
    }
    
    public HashMap<String, Jogador> getJogadores(){
        return this.time;
    }
    
    public void addJogador(String chave, Jogador jogador){
        this.time.put(chave, jogador);
    }
}
